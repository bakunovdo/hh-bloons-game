const GAME_WIDTH = 500;
const GL_MARGIN = 100; // px

const PLAYER_WIDTH = 25;
const PLAYER_HEIGHT = 50;

const BLOON_WIDTH = 25;
const BLOON_HEIGHT = 50;
const FPS_GAME = 120;
const GAME_DURATUION = 60; // seconds

const WIND_MULTIPLICATOR = 0.5;
const DEBUG_WIND_POWER = null; // set null/undefined to disable, or number value for test wind
const BLOONS_COLORS = ["#9c5954", "#9ccc7e", "#817ecc", "#a23cde", "#db3cde"];

const _MS_IN_SECONDS = 1000;

let fpsInterval, startTime, now, then, elapsed;

const getRandomColor = () => {
  const idx = Math.floor(Math.random() * BLOONS_COLORS.length);
  return BLOONS_COLORS[idx];
};

const canvas = document.getElementById("app");
const body = document.querySelector("body");
const scoreValue = document.querySelector(".score__value");
const missValue = document.querySelector(".miss__value");
const timerValue = document.querySelector(".timer__value");

const windPowerLeft = document.querySelector(".wind__power-value__left");
const windPowerRight = document.querySelector(".wind__power-value__right");

const debugValue = document.querySelector(".debug__value");
/** @type {CanvasRenderingContext2D} ctx */
const ctx = canvas.getContext("2d");

body.style.margin = GL_MARGIN + "px";

const setCanvasSize = () => {
  canvas.width = GAME_WIDTH || document.documentElement.clientWidth;
  canvas.height = document.documentElement.clientHeight - GL_MARGIN * 2;
};

const setGameSize = () => {
  gameWidth = canvas.width;
  gameHeight = canvas.height;
};

addEventListener("resize", () => {
  setCanvasSize();
  setGameSize();
});

setTextContent = (element, value) => {
  if (element) {
    element.textContent = value;
  }
};

const setWindPower = (value) => {
  if (value > 0) {
    windPowerLeft.textContent = "";
    windPowerRight.textContent = ">".repeat(value);
    return;
  }

  if (value < 0) {
    windPowerRight.textContent = "";
    windPowerLeft.textContent = "<".repeat(Math.abs(value));
    return;
  }

  windPowerLeft.textContent = "";
  windPowerRight.textContent = "";
};

class Player {
  #keysPressed = {};
  #moveLeft = ["a", "ArrowLeft"];
  #moveRight = ["d", "ArrowRight"];
  #playerDX = 10;
  #stop = false;

  constructor(x, y, w, h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }

  drawXYPoint() {
    ctx.beginPath();
    ctx.arc(this.x, this.y + PLAYER_HEIGHT, 5, 0, Math.PI * 2);
    ctx.fillStyle = "red";
    ctx.fill();
    ctx.closePath();
  }

  draw() {
    const { x, y, w, h } = this;
    const hw = w / 2;
    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.moveTo(x - hw, y);
    ctx.lineTo(x + hw, y);
    ctx.moveTo(x, y + h);
    ctx.lineTo(x + hw, y);
    ctx.moveTo(x, y + h);
    ctx.lineTo(x - hw, y);
    ctx.fill();
    ctx.stroke();
    ctx.closePath();
  }

  onKeyDown(key) {
    this.#keysPressed[key] = true;
  }

  onKeyUp(key) {
    delete this.#keysPressed[key];
  }

  setStop(value) {
    this.#stop = value;
  }

  move() {
    if (!this.#stop) {
      for (const key in this.#keysPressed) {
        if (this.#moveLeft.includes(key)) {
          if (this.x > PLAYER_WIDTH - this.#playerDX) {
            this.x -= this.#playerDX;
          } else {
            this.x = PLAYER_WIDTH / 2;
          }
        }
        if (this.#moveRight.includes(key)) {
          if (this.x < GAME_WIDTH - PLAYER_WIDTH + this.#playerDX) {
            this.x += this.#playerDX;
          } else {
            this.x = GAME_WIDTH - PLAYER_WIDTH / 2;
          }
        }
      }
    }
  }

  render() {
    this.move();
    this.draw();
  }
}

class Bloon {
  #dy = 2;
  #timerModifier = 0;
  #windModifier = 0;

  constructor(x, y, w, h, color) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.color = color;
  }

  move() {
    this.y -= this.#dy + this.#timerModifier;
    // wind power
    let offset = this.#windModifier > 0 ? gameWidth - this.x : this.x;
    offset = Math.floor(offset);
    const posXModifier = offset / gameWidth;
    const dX = posXModifier * this.#windModifier;
    const newX = this.x + dX;
    // setTextContent(debugValue, `offset ${offset}`);
    if ((dX < 0 && newX < BLOON_WIDTH) || (dX > 0 && newX > gameWidth - BLOON_WIDTH)) {
      return;
    }

    this.x = newX;
  }

  drawXYPoint() {
    ctx.beginPath();
    ctx.arc(this.x, this.y - this.h, 5, 0, Math.PI * 2);
    ctx.fillStyle = "red";
    ctx.fill();
    ctx.closePath();
  }

  draw() {
    const { x, y, w, h, color } = this;
    this.move();
    ctx.beginPath();
    ctx.ellipse(x, y, w, h, 0, Math.PI * 2, 0);
    ctx.fillStyle = color;
    ctx.lineWidth = 0.75;
    ctx.fill();
    ctx.stroke();
  }

  // ms
  setTimerModifier(value) {
    this.#timerModifier = value;
  }

  // -9 to 9
  setWindModifier(value) {
    this.#windModifier = value;
  }

  render() {
    this.draw();
  }
}

class Game {
  //Canvas
  #fps = 60;

  //Global
  #globalIntervalId = null;
  #animationId = null;
  isRunning = false;

  //Timer
  #stopIn = GAME_DURATUION * _MS_IN_SECONDS;
  #stopTime = 0;
  #start = true;
  #stop = false;
  #timeTillStop = 0;

  // In game
  #startGameOver = false;
  #endGameOver = false;
  #bloons = [];
  #player = null;
  #score = 0;
  #miss = 0;

  // Modifers
  //    Speed Up
  #bloonsDYModifier = 0;
  //    Wind Power
  #windPower = 0;

  constructor(fps) {
    this.#fps = fps || 60;

    this.animate = this.animate.bind(this);

    this.init();
  }

  preInit() {
    setCanvasSize();
    setGameSize();
  }

  #spawnPlayer() {
    this.#player = new Player(gameWidth / 2, 0, PLAYER_WIDTH, PLAYER_HEIGHT);
  }

  #initPlayerListeners() {
    addEventListener("keydown", (event) => {
      this.#player.onKeyDown(event.key);
    });

    addEventListener("keyup", (event) => {
      this.#player.onKeyUp(event.key);
    });
  }

  init() {
    this.preInit();
    this.#spawnPlayer();
    this.#initPlayerListeners();
  }

  reset() {
    this.#bloons = [];
    this.#score = 0;
    this.#miss = 0;
    this.#bloonsDYModifier = 0;
    this.#windPower = 0;

    setWindPower(0);
    setTextContent(missValue, 0);
    setTextContent(scoreValue, 0);
  }

  clearGlobalInterval() {
    if (this.#globalIntervalId !== undefined || this.#globalIntervalId !== null) {
      clearInterval(this.#globalIntervalId);
      this.#globalIntervalId = null;
    }
  }

  createGameSpawner() {
    this.clearGlobalInterval();

    this.#globalIntervalId = setInterval(() => {
      const pastTime = Math.abs(this.#stopIn - this.#timeTillStop);
      const seconds = Math.floor(pastTime / _MS_IN_SECONDS);
      this.#bloonsDYModifier = Math.floor(pastTime / (5 * _MS_IN_SECONDS)) / 2; // every 5 seconds, speed up on 25%;

      if (Math.floor((seconds / 6) % 1 === 0)) {
        // every 6 seconds change wind power
        this.randomWindLevel();
      }

      if (pastTime > 50 * _MS_IN_SECONDS) {
        this.spawnBloon();
        this.spawnBloon(300);
      } else if (pastTime > 40 * _MS_IN_SECONDS) {
        if (seconds % 2 === 0) {
          this.spawnBloon();
          this.spawnBloon(600);
        }
      } else if (pastTime > 30 * _MS_IN_SECONDS) {
        if (seconds % 2 === 0) {
          this.spawnBloon();
          this.spawnBloon(600);
        }
      } else if (pastTime > 20 * _MS_IN_SECONDS) {
        if (seconds % 2 === 0) {
          this.spawnBloon();
        }
        if (seconds % 3 === 0) {
          this.spawnBloon();
          this.spawnBloon(500);
        }
      } else if (pastTime > 10 * _MS_IN_SECONDS) {
        if (seconds % 2 === 0 || seconds % 3 === 0) {
          this.spawnBloon();
        }
      } else {
        if (seconds % 3 === 0) {
          this.spawnBloon();
        }
      }
    }, 1000);
  }

  spawnBloon(delay = 0) {
    const x = Math.random() * (gameWidth - BLOON_WIDTH * 2) + BLOON_WIDTH;
    const color = getRandomColor();

    setTimeout(() => {
      this.#bloons.push(new Bloon(x, gameHeight + BLOON_HEIGHT, BLOON_WIDTH, BLOON_HEIGHT, color));
    }, delay);
  }

  randomWindLevel() {
    const value = Math.floor(Math.random() * 10);
    const dir = Math.random() >= 0.5 ? 1 : -1;
    this.#windPower = DEBUG_WIND_POWER ?? value * dir;
    setWindPower(this.#windPower);
  }

  async BloonsAnimation() {
    for (let i = 1; i < 100; i++) {
      this.spawnBloon(Math.random() * 500);
    }

    return new Promise((resolve) => {
      this.#globalIntervalId = setInterval(() => {
        if (this.#bloons.length === 0) {
          resolve();
          clearInterval(this.#globalIntervalId);
          this.#globalIntervalId = null;
        }
      }, 50);
    });
  }

  renderScore() {
    const rectX = 100;
    ctx.rect(rectX, gameHeight / 2 - 125, gameWidth - rectX * 2, 225);
    ctx.fillStyle = "black";
    ctx.fill();
    ctx.stroke();

    ctx.font = "normal bold 36px serif";
    ctx.textAlign = "center";
    ctx.fillStyle = "white";
    ctx.fillText(`Time over!`, gameWidth / 2, gameHeight / 2 - 48 - 16);
    ctx.fillText(`Score: ${this.#score}`, gameWidth / 2, gameHeight / 2);
    ctx.fillText(`Miss: ${this.#miss}`, gameWidth / 2, gameHeight / 2 + 48 + 16);
  }

  async gameOver() {
    this.#startGameOver = true;

    this.clearGlobalInterval();
    setTextContent(timerValue, 0);
    await this.BloonsAnimation();

    this.#endGameOver = true;
  }

  stopGame() {
    cancelAnimationFrame(this.#animationId);
  }

  deleteBloon(index, isMiss) {
    setTimeout(() => {
      this.#bloons.splice(index, 1);
      if (isMiss) {
        this.#miss += 1;
        setTextContent(missValue, this.#miss);
      }
    }, 0);
  }

  renderBloons() {
    this.#bloons.forEach((bloon, index) => {
      if (this.#stop) {
        bloon.setTimerModifier(15);
        bloon.setWindModifier(0);
        setWindPower(0);
      } else {
        bloon.setTimerModifier(this.#bloonsDYModifier);
        bloon.setWindModifier(this.#windPower * WIND_MULTIPLICATOR);
      }

      bloon.render();

      const distX = this.#player.x - bloon.x;
      const distY = this.#player.y - bloon.y + BLOON_HEIGHT;
      const colisX = Math.abs(distX) <= BLOON_WIDTH;
      const colisY = Math.abs(distY) >= -PLAYER_HEIGHT / 2 && Math.abs(distY) <= PLAYER_HEIGHT / 2;

      if (bloon.y + BLOON_HEIGHT * 2 <= 0) {
        this.deleteBloon(index, !this.#stop);
      }

      if (colisX && colisY) {
        setTimeout(() => {
          if (!this.#stop) {
            this.deleteBloon(index);
            this.#score += 1;
          }
          setTextContent(scoreValue, this.#score);
        }, 0);
      }
    });
  }

  renderPlayer() {
    if (this.#stop) {
      this.#player.setStop(true);
    }

    this.#player.render();
  }

  render() {
    ctx.clearRect(0, 0, gameWidth, gameHeight);
    this.renderPlayer();
    this.renderBloons();
    if (this.#endGameOver) {
      this.renderScore();
    }
  }

  startAnimating() {
    fpsInterval = 1000 / this.#fps;
    then = Date.now();
    startTime = then;
    this.animate();
  }

  animate(timer) {
    this.#animationId = requestAnimationFrame(this.animate);

    if (timer && this.#start) {
      this.#stopTime = timer + this.#stopIn;
      this.#start = false;
    } else if (timer + 1 * _MS_IN_SECONDS >= this.#stopTime) {
      this.#stop = true;
    }

    this.#timeTillStop = this.#stopTime - timer;

    now = Date.now();
    elapsed = now - then;
    if (elapsed > fpsInterval) {
      then = now - (elapsed % fpsInterval);
      const seconds = Math.floor(this.#timeTillStop / _MS_IN_SECONDS);

      if (this.#stop && !this.#startGameOver) {
        this.gameOver();
      }

      if (!this.#stop) {
        setTextContent(timerValue, seconds);
      }

      this.render();
    }
  }

  start() {
    this.startAnimating();
    this.createGameSpawner();
    this.isRunning = true;
  }
}

const game = new Game(FPS_GAME);

game.start();
